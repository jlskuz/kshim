Simple application wrapper

```
> kshimgen
KShimgen 0.6.0:
--create shim target                    Create a shim
--env key=val                           additional environment varriables for the shim
--enable-env-override                   whether to allow overriding the target with the env var KSHIM_shim
--gui                                   create a gui application (only supported on Windows)
-- arg1 arg2 arg3...                    arguments that get passed to the target
```
